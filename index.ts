export interface Game {
  gameId: number
  gameName: string
  shortName: string
  subtitle: string
  modes?: Mode[]
}

export interface Mode {
  modeId: number
  gameId: number
  modeName: string
  link: null | string
  sortOrder: number
  gradeRequired: FieldRequired
  scoreRequired: FieldRequired
  levelRequired: FieldRequired
  timeRequired: FieldRequired
  tags?: ModeTag[]
  submissionRange?: SubmissionRange
}

export enum FieldRequired {
  Hidden = 'hidden',
  Optional = 'optional',
  Required = 'required',
}

export interface SubmissionRange {
  end?: Date
  start?: Date
}

export enum ModeTag {
  Carnival = 'CARNIVAL',
  Doom = 'DOOM',
  Event = 'EVENT',
  Extended = 'EXTENDED',
  Main = 'MAIN',
}

export interface Player {
  playerId: number
  playerName: string
  avatar?: string
  location?: string
}

export interface GameRankingModel {
  rankId: number
  gameId: number
  player: Player
  scoreCount: number
  rank: number
  rankingPoints: number
}

export interface ModeRankingModel {
  scoreId: number
  gameId: number
  modeId: number
  player?: Player
  level?: number
  playtime?: string
  grade?: GradeModel
  proof?: ProofModel[]
  comment?: string
  score?: number
  rank: number
  rankingPoints: number
  status: ScoreStatus
  createdAt: string
  verification?: Verification
}

export enum GradeLine {
  Orange = 'orange',
  Green = 'green',
}
export interface GradeModel {
  gradeId: number
  gradeDisplay: string
  gradeOrder: number
  line?: GradeLine
}

export enum ProofType {
  Video = 'video',
  Image = 'image',
  Other = 'other',
}

export interface ProofModel {
  type: ProofType
  link: string
}

export interface Verification {
  comment: null | string
  verifiedBy: Player
  verifiedOn: Date
}

export enum ScoreStatus {
  Unverified = 'unverified',
  Pending = 'pending',
  Legacy = 'legacy',
  Rejected = 'rejected',
  Verified = 'verified',
  Accepted = 'accepted',
}

export interface RecentActivityModel {
  gameId: number
  modeId: number
  score: ModeRankingModel
  previousScore: ModeRankingModel | undefined
  delta: Delta
}

export interface Delta {
  overall: DeltaDetails
  game: DeltaDetails
  mode: DeltaDetails
}

export interface DeltaDetails {
  rank: number | null
  rankingPoints: number | null
  previousRank: number | null
  previousRankingPoints: number | null
  playersOffset: Player[]
}

export interface Location {
  alpha2: string
  alpha3: string
  name: string
}
